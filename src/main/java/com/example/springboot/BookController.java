package com.example.springboot;

import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/books")
public class BookController {

    @GetMapping
    public List<Book> all(){
        List<Book> bookList = List.of(new Book("Harry Potter", "J.K. Rowling"), new Book("Lord of the Rings", "Tolkien"), new Book("Game of Thrones", "Martin"), new Book ("The Hobbit", "Tolkien"));
        return bookList;
    }

    @PostMapping
    public Book createBook(@RequestBody CreateBook createBook){
        return new Book(createBook.getName(), createBook.getAuthor());
    }

    @GetMapping("/{id}")
    public Optional<Book> getBookById(@PathVariable("id") String id){
        List<Book> bookList = all();

        Optional<Book> bookById = bookList
                .stream()
                .filter(book -> book.getId() == id)
                .findAny();
        return bookById;
    }

    @PutMapping("/{id}")
    public Book updateBook(@PathVariable ("id") String id, @RequestBody UpdateBook updateBook){
        Book book = null;
        return book;
    }

    @DeleteMapping("/{id}")
    public void deleteBook(@PathVariable("id") String id){
        List<Book> bookToDelete = all();

    }
}
