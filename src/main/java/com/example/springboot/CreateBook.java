package com.example.springboot;

import lombok.Value;

@Value
public class CreateBook {
    String name;
    String author;
}
