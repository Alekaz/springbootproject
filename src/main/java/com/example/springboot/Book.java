package com.example.springboot;

import lombok.Value;

import java.util.UUID;

@Value
public class Book {

    String name;
    String author;
    String id;

    Book(String name, String author){
        this.name = name;
        this.author = author;
        this.id = UUID.randomUUID().toString();
    }

}
