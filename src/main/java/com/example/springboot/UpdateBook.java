package com.example.springboot;

import lombok.Value;

@Value
public class UpdateBook {
    String name;
    String author;
}
